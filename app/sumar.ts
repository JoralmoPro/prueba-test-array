function sumarArray(datos: Array<number>, res: any = null): number {
  let suma: number = 0;
  let arrTemp: any = [];
  if (datos[0].constructor == Array) {
    for (let dato of datos) {
      arrTemp = dato;
      try {
        suma = arrTemp.reduce((a: any, b: any) => a + b, suma);
      } catch (error) {
        return res.status(500).json({
          response: {
            data: "",
            errors: ["internal_server_error"]
          }
        });
      }
    }
  } else {
    suma = datos.reduce((a, b) => {
      return b.constructor != Array
        ? a + b
        : res
            .status(500)
            .json({
              response: { data: "", errors: ["internal_server_error"] }
            });
    }, 0);
  }
  return suma;
}

export default sumarArray;
