function multiplicarArray(datos: Array<number>, res: any = null): number {
  let multiplicacion: number = 1;
  let arrTemp: any = [];
  if (datos[0].constructor == Array) {
    for (let dato of datos) {
      arrTemp = dato;
      try {
        multiplicacion = arrTemp.reduce((a: any, b: any) => a * b, multiplicacion);
      } catch (error) {
        return res.status(500).json({
          response: {
            data: "",
            errors: ["internal_server_error"]
          }
        });
      }
    }
  } else {
    multiplicacion = datos.reduce((a, b) => {
      return b.constructor != Array
        ? a * b
        : res
            .status(500)
            .json({
              response: { data: "", errors: ["internal_server_error"] }
            });
    }, 1);
  }
  return multiplicacion;
}

export default multiplicarArray;
