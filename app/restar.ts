function restarArray(datos: Array<number>, res: any = null): number {
  let resta: number = 0;
  let arrTemp: any = [];
  if (datos[0].constructor == Array) {
    for (let dato of datos) {
      arrTemp = dato;
      arrTemp.map((n: number, i: number) => {
        resta = resta - n;
      });
    }
  } else {
    datos.map((n: number, i: number) => {
      resta = resta - n;
    });
  }
  return resta;
}

export default restarArray;
