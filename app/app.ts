import express from "express";
import sumarArray from "./sumar";
import multiplicarArray from "./multiplicar";
import restarArray from "./restar";
import dividirArray from "./division";

const app = express();
const puerto = process.env.PORT || 8181;

app.use(express.json());

app.get("/", (req: any, res: any) => {
  res.send("Hola mundo");
});

app.post("/test", (req: any, res: any) => {
  const { data } = req.body;
  if (!Array.isArray(data))
    res.status(422).json({
      response: {
        data: "",
        errors: ["invalid_data_format"]
      }
    });
  else {
    let suma = sumarArray(data, res);
    let multiplicacion = multiplicarArray(data, res);
    let resta = restarArray(data, res);
    let division = dividirArray(data, res);
    res.status(200).json({
      response: {
        data: {
          suma,
          resta,
          multiplicacion,
          division
        },
        errors: []
      }
    });
  }
});

app.listen(puerto, () => {
  console.log(`http://localhost:${puerto}`);
});
