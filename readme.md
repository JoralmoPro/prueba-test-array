### Api Test Array

### Link para probar online: https://pruebatestsarray.herokuapp.com/

#### Clonar el código
```sh
    git clone https://JoralmoPro@bitbucket.org/JoralmoPro/prueba-test-array.git
    cd nombre
```

#### Instalar las dependencias
```sh
    npm install
```

#### Para probar el proyecto
```sh
    npm run dev
```
    > http://localhost:8181
    

#### Una vez ejecutado se tiene el endpoint

    http://localhost:8181/test
    
    enviar el los parametros:
    
    {
        data: [1, 2, 3, 4]
    }
   
#### Para probar en producción ```npm run prod```

#### Generar el build ```npm run tsc```
    
#### Correr el test
```sh
    npn run test
```