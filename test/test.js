const chai = require("chai");
const http = require("chai-http");
const sumarArray = require("./../build/sumar").default;
const restarArray = require("./../build/restar").default;
const multiplicarArray = require("./../build/multiplicar").default;
const divisionArray = require("./../build/division").default;
chai.use(http);

describe("Suma", () => {
  it("Para verificar la suma del array", done => {
    const array = [1, 2, 3];
    const suma = sumarArray(array);
    chai.assert.equal(suma, 6, "El resultado no es correcto");
    done();
  });
});

describe("Resta", () => {
  it("Para verificar la resta del array", done => {
    const array = [1, 2, 3];
    const resta = restarArray(array);
    chai.assert.equal(resta, -6, "El resultado no es correcto");
    done();
  });
});

describe("Multiplicación", () => {
  it("Para verificar la multiplicacion del array", done => {
    const array = [1, 2, 3, 4];
    const multiplicacion = multiplicarArray(array);
    chai.assert.equal(multiplicacion, 24, "El resultado no es correcto");
    done();
  });
});

describe("Division", () => {
  it("Para verificar la division del array", done => {
    const array = [1, 2, 3, 4];
    const division = divisionArray(array);
    chai.assert.equal(
      division,
      0.041666666666666664,
      "El resultado no es correcto"
    );
    done();
  });
});

describe("Test endpoint", () => {
  it("Para verificar si el endponit responde correctamente", done => {
    const array = [1, 2, 3, 4];
    chai
      .request(`http://localhost:8181`)
      .post("/test")
      .send({ data: array })
      .then(res => {
        const { data } = res.body.response;
        chai.assert.equal(res.statusCode, 200);
        chai.assert.equal(data.suma, 10);
        chai.assert.equal(data.resta, -10);
        chai.assert.equal(data.multiplicacion, 24);
        chai.assert.equal(data.division, 0.041666666666666664);
        done();
      });
  });
});
